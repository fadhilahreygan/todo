const app = require("../app.js");
const request = require("supertest");
const { sequelize } = require("../models");
const { queryInterface } = sequelize;

// CODE SEBELUM TESTING
// ==> BULK INSERT / POPULATE DATA

beforeAll((done) => {
  queryInterface
    .bulkInsert(
      "toDos",
      [
        {
          id: 1001,
          title: "Self Learning",
          description: "Menonton video materi pada LMS",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 1002,
          title: "Homework",
          description: "Mengerjakan homework",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 1003,
          title: "Exam",
          description: "Mengerjakan exam minggu ini",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    )
    .then((_) => {
      done();
    })
    .catch((err) => {
      done(err);
    });
});

// CODE SETELAH TESTING SELESAI
// CLEANING DATABASE / HAPUS SEMUA DATA DI TABLE toDo

afterAll((done) => {
  queryInterface
    .bulkDelete("toDos", null, {})
    .then((_) => {
      done();
    })
    .catch((err) => {
      done(err);
    });
});

// UNIT TESTING / TEST DRIVEN DEVELOPMENT
// TEST SETIAP ENDPOINT YANG DIBUAT

describe("GET /toDo", () => {
  it("Get list toDo", (done) => {
    request(app)
      .get("/toDo")
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        const { data } = res.body;
        expect(data.length).toBe(3);
        const firstData = data[0];

        expect(firstData.title).toEqual("Self Learning");
        expect(firstData.id).toEqual(1001);
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});

describe("GET /toDo/:id", () => {
  it("Get ToDo Detail", (done) => {
    request(app)
      .get(`/toDo/${1002}`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        const { data } = res.body;
        expect(data.id).toEqual(1002);
        expect(data.title).toEqual("Homework");
        expect(data.description).toEqual("Mengerjakan homework");
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("Error Not Found", (done) => {
    request(app)
      .get(`/toDo/${6666}`)
      .expect("Content-Type", /json/)
      .expect(404)
      .then((res) => {
        const { message } = res.body;

        expect(message).toEqual("Error Not Found");
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});

describe("POST /toDo", () => {
  it("Create ToDo successfully", (done) => {
    request(app)
      .post("/toDo")
      .send({
        title: "Belajar",
        description: "Waktunya belajar docker",
      })
      .expect("Content-Type", /json/)
      .expect(201)
      .then((res) => {
        const { message, data } = res.body;

        expect(message).toEqual("New ToDo created successfully");
        expect(data.title).toEqual("Belajar");
        expect(data.description).toEqual("Waktunya belajar docker");
        done();
      })
      .catch((err) => {
        done(err);
      });
  });

  it("Validation Error", (done) => {
    request(app)
      .post("/toDo")
      .send({
        description: "Waktunya belajar docker",
      })
      .expect("Content-Type", /json/)
      .expect(400)
      .then((res) => {
        const { name, message } = res.body;

        expect(name).toBe("SequelizeValidationError");
        expect(message).toEqual("notNull Violation: toDo.title cannot be null");
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});

describe("PUT /toDo/:id", () => {
  it("ToDo updated successfully", (done) => {
    request(app)
      .put(`/toDo/${1002}`)
      .send({
        title: "Homework",
        description: "Mengerjakan homework docker",
      })
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        const { message, data } = res.body;

        expect(message).toEqual("ToDo updated successfully");
        expect(data.title).toBe("Homework");
        expect(data.description).toBe("Mengerjakan homework docker");
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});

describe("DELETE /toDo/:id", () => {
  it("ToDo deleted successfully", (done) => {
    request(app)
      .delete(`/toDo/${1003}`)
      .expect("Content-Type", /json/)
      .expect(200)
      .then((res) => {
        const { message } = res.body;

        expect(message).toEqual("ToDo Deleted Successfully");
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});
