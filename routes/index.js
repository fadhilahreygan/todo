const express = require("express");
const router = express.Router();
const toDoController = require("../controller/toDoController.js");

router.get("/toDo", toDoController.findAll);
router.get("/toDo/:id", toDoController.findOne);
router.post("/toDo", toDoController.create);
router.put("/toDo/:id", toDoController.update);
router.delete("/toDo/:id", toDoController.destroy);

module.exports = router;
