"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "toDos",
      [
        {
          title: "Self Learning",
          description: "Menonton video materi pada LMS",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Homework",
          description: "Mengerjakan homework",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          title: "Exam",
          description: "Mengerjakan exam minggu ini",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("toDos", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
