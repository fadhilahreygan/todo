const { toDo } = require("../models");

class toDoController {
  static findAll = async (req, res, next) => {
    try {
      const ToDo = await toDo.findAll();
      res.status(200).json({ data: ToDo });
    } catch (err) {
      next(err);
    }
  };

  static findOne = async (req, res, next) => {
    try {
      const { id } = req.params;

      const ToDo = await toDo.findOne({
        where: {
          id,
        },
      });

      if (!ToDo) {
        throw { name: "ErrorNotFound" };
      }

      res.status(200).json({ data: ToDo });
    } catch (err) {
      next(err);
    }
  };

  static create = async (req, res, next) => {
    try {
      const { title, description } = req.body;

      const ToDo = await toDo.create(
        {
          title,
          description,
        },
        { returning: true }
      );

      res.status(201).json({
        message: "New ToDo created successfully",
        data: ToDo,
      });
    } catch (err) {
      next(err);
    }
  };

  static update = async (req, res, next) => {
    try {
      const { title, description } = req.body;
      const { id } = req.params;

      const ToDo = await toDo.findOne({
        where: {
          id,
        },
      });

      if (!ToDo) {
        throw { name: "ErrorNotFound" };
      }

      await ToDo.update({
        title: title || ToDo.title,
        description: description || ToDo.description,
      });

      res.status(200).json({
        message: "ToDo updated successfully",
        data: ToDo,
      });
    } catch (err) {
      next(err);
    }
  };

  static destroy = async (req, res, next) => {
    try {
      const { id } = req.params;

      const ToDo = await toDo.findOne({
        where: {
          id,
        },
      });

      if (!ToDo) {
        throw { name: "ErrorNotFound" };
      }

      await ToDo.destroy();

      res.status(200).json({ message: "ToDo Deleted Successfully" });
    } catch (err) {
      next(err);
    }
  };
}

module.exports = toDoController;
